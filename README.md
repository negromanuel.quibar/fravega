#  Fravega - Challenge 1 - Sucursal CRUD

## Overview
Proyecto Maven creado por Manuel Quibar,

Java: 11

Spring Boot: 2.5.2

Se utiliza H2 en memoria como motor de base de datos se puede acceder a la consola mediante http://localhost:8080/h2-console/

## Dependencies
JPA

Security

Validation

Lombok

H2 DataBase

Swagger

Apache Commons

## Swagger

Se puede acceder al entorno swagger mediante host:8080/swagger-ui.html#
