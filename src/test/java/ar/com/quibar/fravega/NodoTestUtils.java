package ar.com.quibar.fravega;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import ar.com.quibar.fravega.nodo.domain.model.Nodo;
import ar.com.quibar.fravega.nodo.domain.model.PuntoRetiro;
import ar.com.quibar.fravega.nodo.domain.model.Sucursal;

public class NodoTestUtils extends TestUtils{

	public static Sucursal buildSucursal() {
		final var sucursal = new Sucursal();
		sucursal.setDireccion(RandomStringUtils.random(RANDOM_STRING_LENGTH));
		sucursal.setHorarioAtencion(RandomStringUtils.random(RANDOM_STRING_LENGTH));
		sucursal.setLatitud(RandomUtils.nextLong());
		sucursal.setLongitud(RandomUtils.nextLong());
		return sucursal;
	}
	
	public static PuntoRetiro buildPuntoRetiro() {
		final var ptoRetiro = new PuntoRetiro();
		ptoRetiro.setCapacidad(RandomUtils.nextLong());
		ptoRetiro.setLatitud(RandomUtils.nextLong());
		ptoRetiro.setLongitud(RandomUtils.nextLong());
		return ptoRetiro;
	}
	
	public static List<Nodo> buildNodos(){
		final List<Nodo> nodos = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			nodos.add(buildSucursal());
			nodos.add(buildPuntoRetiro());
		}
		return nodos;
	}
}
