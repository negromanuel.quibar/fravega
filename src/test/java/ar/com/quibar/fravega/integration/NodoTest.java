package ar.com.quibar.fravega.integration;

import static ar.com.quibar.fravega.TestUtils.parseRequest;
import static ar.com.quibar.fravega.TestUtils.parseResponse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import ar.com.quibar.fravega.NodoTestUtils;
import ar.com.quibar.fravega.nodo.domain.dto.NodoConDistancia;
import ar.com.quibar.fravega.nodo.domain.model.Sucursal;
import ar.com.quibar.fravega.nodo.infrastructure.dto.SucursalRequest;
import ar.com.quibar.fravega.nodo.infrastructure.repository.JpaSqlNodoRepository;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class NodoTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	JpaSqlNodoRepository repository;
	
	@BeforeEach
	void setUp() throws Exception {
		final var nodos = NodoTestUtils.buildNodos();
		repository.saveAll(nodos);
	}


	@Test
	void createNodoest() throws Exception {
		final var request = SucursalRequest.builder()
				.direccion("direccion")
				.horarioAtencion("De - Hasta - ")
				.longitud(RandomUtils.nextLong())
				.latitud(RandomUtils.nextLong())
				.build();
		MvcResult requestResult = mockMvc.perform(
					post("/nodo/sucursal")
					.content(parseRequest(request))
					.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andReturn();
		final var response = parseResponse(requestResult, Sucursal.class);
		assertNotNull(response);
		assertNotNull(response.getId());
	}

	@Test
	void createNodoWorgnTest() throws Exception {
		final var request = SucursalRequest.builder()
				.horarioAtencion("De - Hasta - ")
				.longitud(RandomUtils.nextLong())
				.latitud(RandomUtils.nextLong())
				.build();
		mockMvc.perform(
					post("/nodo/sucursal")
					.content(parseRequest(request))
					.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().is4xxClientError());
	}
	
	@Test
	void getNearTest() throws Exception {
		MvcResult requestResult = mockMvc.perform(
					get("/nodo/near")
					.param("lat", String.valueOf(RandomUtils.nextLong()))
					.param("lng", String.valueOf(RandomUtils.nextLong())))
				.andExpect(status().isOk())
				.andReturn();
		final var response = parseResponse(requestResult, NodoConDistancia.class);
		assertNotNull(response);
	}
}
