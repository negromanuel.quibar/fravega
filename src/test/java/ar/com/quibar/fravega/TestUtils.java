package ar.com.quibar.fravega;

import java.io.IOException;

import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class TestUtils {

    public static final int RANDOM_STRING_LENGTH = 20;
    public static final int RANDOM_NUMBER_LENGTH = 10;
    
	private static final ObjectMapper MAPPER = new ObjectMapper()
		    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
		    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
		    .registerModule(new JavaTimeModule());
	
	public static <T> T parseResponse(MvcResult result, Class<T> responseClass) {
	    try {
	      String contentAsString = result.getResponse().getContentAsString();
	      return MAPPER.readValue(contentAsString, responseClass);
	    } catch (IOException e) {
	      throw new RuntimeException(e);
	    }
	}
	
	public static String parseRequest(Object objRequest) {
		try {
			return MAPPER.writeValueAsString(objRequest);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
