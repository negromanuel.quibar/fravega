package ar.com.quibar.fravega;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FravegaChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(FravegaChallengeApplication.class, args);
	}

}
