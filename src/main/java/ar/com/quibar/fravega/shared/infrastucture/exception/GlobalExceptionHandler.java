package ar.com.quibar.fravega.shared.infrastucture.exception;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.RequiredArgsConstructor;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler  {
	
	public static final String CODE = "code";
	public static final String MESSAGE = "message";
	public static final String STATUS = "status";
	public static final String FIELDS = "fields";
	public static final String CAMPOS_INVALIDOS = "Campos invalidos";
	
	@ExceptionHandler(Exception.class)
	ResponseEntity<Object> handleException(Exception ex, Object body, WebRequest request) {
		ex.printStackTrace();
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}
	
	
	@ExceptionHandler(ResponseStatusException.class)
	public final ResponseEntity<ExceptionResponse> handleValidatoinException(ResponseStatusException ex, WebRequest request) {

		final var response = ExceptionResponse.builder()
				.message(ex.getMessage())
				.dateTime(LocalDateTime.now())
				.build();

		return new ResponseEntity<>(response, ex.getStatus());
	}
	
	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		Map<String, Object> body = new LinkedHashMap<>();
		body.put(CODE, -1);
		body.put(MESSAGE, CAMPOS_INVALIDOS);

		List<FieldValidation> fields = ex.getBindingResult().getFieldErrors().stream()
				.map(x -> {
					try {
						return new FieldValidation(x.getField(), x.getDefaultMessage() );
					} catch (Exception e) {
			            return new FieldValidation(x.getField(), x.getDefaultMessage());
			        }
				}).collect(Collectors.toList());

		body.put(FIELDS, fields);

		return new ResponseEntity<>(body, headers, status);
	}
	
}
