package ar.com.quibar.fravega.shared.infrastucture.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.web.server.ResponseStatusException;

public class NotFoundResourceException extends ResponseStatusException {

	private static final long serialVersionUID = 1L;

	public NotFoundResourceException()
    {
        super(NOT_FOUND);
    }

    public NotFoundResourceException(String message)
    {
        super(NOT_FOUND, message);
    }
    
    public NotFoundResourceException(String message, Throwable cause) {
        super(NOT_FOUND, message, cause);
    }
}
