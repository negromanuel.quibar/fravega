package ar.com.quibar.fravega.nodo.infrastructure.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class NodoRequest {

	@NotNull
	private Long latitud;
	@NotNull
	private Long longitud;
	
}
