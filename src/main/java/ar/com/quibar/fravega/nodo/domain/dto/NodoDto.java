package ar.com.quibar.fravega.nodo.domain.dto;

public abstract class NodoDto {
	
	protected long latitud;
	protected long longitud;
	
	public long getLatitud() {
		return latitud;
	}
	public long getLongitud() {
		return longitud;
	}
	
	public void setLatitud(long latitud) {
		this.latitud = latitud;
	}
	public void setLongitud(long longitud) {
		this.longitud = longitud;
	}
	
	

}
