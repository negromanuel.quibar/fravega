package ar.com.quibar.fravega.nodo.domain.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="nodo")
@DiscriminatorValue(value = "sucursal")
@DiscriminatorColumn(name = "nodo_type")
public class Sucursal extends Nodo {

	@Column
	private String direccion;
	
	@Column(name = "horario_atencion")
	private String horarioAtencion;

	
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getHorarioAtencion() {
		return horarioAtencion;
	}

	public void setHorarioAtencion(String horarioAtencion) {
		this.horarioAtencion = horarioAtencion;
	}
	
	
}
