package ar.com.quibar.fravega.nodo.domain.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="nodo")
@DiscriminatorValue(value = "punto_retiro")
@DiscriminatorColumn(name = "nodo_type")
public class PuntoRetiro extends Nodo {
	
	@Column
	private Long capacidad;

	public Long getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(Long capacidad) {
		this.capacidad = capacidad;
	}
	
	

}
