package ar.com.quibar.fravega.nodo.infrastructure.dto;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class SucursalRequest extends NodoRequest {

	@NotBlank
	private String direccion;
	
	@NotBlank
	private String horarioAtencion;
}
