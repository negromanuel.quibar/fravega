package ar.com.quibar.fravega.nodo.infrastructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ar.com.quibar.fravega.nodo.domain.usecase.CreateNode;
import ar.com.quibar.fravega.nodo.domain.usecase.DeleteNodo;
import ar.com.quibar.fravega.nodo.domain.usecase.FindNode;
import ar.com.quibar.fravega.nodo.domain.usecase.UpdateNodo;
import ar.com.quibar.fravega.nodo.infrastructure.repository.JpaSqlNodoRepository;

@Configuration
public class NodoConfiguration {

	@Bean
	public FindNode find(JpaSqlNodoRepository repository) {
		return new FindNode(repository);
	}
	
	@Bean
	public DeleteNodo delete(JpaSqlNodoRepository repository) {
		return new DeleteNodo(repository);
	}
	
	@Bean
	public CreateNode create(JpaSqlNodoRepository repository) {
		return new CreateNode(repository);
	}
	
	@Bean
	public UpdateNodo update(JpaSqlNodoRepository repository) {
		return new UpdateNodo(repository);
	}
}
