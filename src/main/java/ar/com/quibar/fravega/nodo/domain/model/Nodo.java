package ar.com.quibar.fravega.nodo.domain.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity(name="nodo")
@Table(name="nodo")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "nodo_type")
public class Nodo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Integer id;
	
	@Column(name = "latitud")
	protected Long latitud;
	
	@Column(name = "longitud")
	protected Long longitud;
	
	public Integer getId() {
		return id;
	}
	public Long getLatitud() {
		return latitud;
	}
	public Long getLongitud() {
		return longitud;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setLatitud(Long latitud) {
		this.latitud = latitud;
	}
	public void setLongitud(Long longitud) {
		this.longitud = longitud;
	}
	
	
}
