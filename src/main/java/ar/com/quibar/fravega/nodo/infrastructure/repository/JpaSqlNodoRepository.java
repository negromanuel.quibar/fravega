package ar.com.quibar.fravega.nodo.infrastructure.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ar.com.quibar.fravega.nodo.domain.model.Nodo;
import ar.com.quibar.fravega.nodo.domain.port.NodoRepository;

@Repository
public interface JpaSqlNodoRepository extends JpaRepository<Nodo, Integer>, NodoRepository<Nodo> {

	@Query(value = " SELECT TOP 1 *, sqrt(power(n.latitud - ?1 , 2 )+power(n.longitud -?2, 2)) as distancia from NODO n order by distancia asc", nativeQuery = true)
	Optional<Nodo> findOneNearby(Long latitud, Long longitud);
}
