package ar.com.quibar.fravega.nodo.domain.dto;

public class PuntoRetiroDto extends NodoDto {
	
	private Long capacidad;

	public Long getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(Long capacidad) {
		this.capacidad = capacidad;
	}
	
	

}
