package ar.com.quibar.fravega.nodo.infrastructure.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PuntoRetiroRequest extends NodoRequest {

	@NotNull
	private Long capacidad;
}
