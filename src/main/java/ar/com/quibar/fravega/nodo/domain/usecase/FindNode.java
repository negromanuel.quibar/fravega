package ar.com.quibar.fravega.nodo.domain.usecase;

import java.util.List;

import ar.com.quibar.fravega.nodo.domain.dto.NodoConDistancia;
import ar.com.quibar.fravega.nodo.domain.model.Nodo;
import ar.com.quibar.fravega.nodo.domain.port.NodoRepository;
import ar.com.quibar.fravega.shared.infrastucture.exception.InvalidResourceException;
import ar.com.quibar.fravega.shared.infrastucture.exception.NotFoundResourceException;

public class FindNode {

	private final NodoRepository<Nodo> repository;

	public FindNode(NodoRepository<Nodo> repository) {
		this.repository = repository;
	}
	
	public Nodo find(Integer id) {
		return repository.findById(id)
				.orElseThrow(() -> new InvalidResourceException("Nodo no encontrado"));
	}
	
	public List<Nodo> find(){
		return repository.findAll();
	}
	
	public NodoConDistancia findNearby(Long latitud, Long longitud) {
		final var nodo = repository.findOneNearby(latitud, longitud)
				.orElseThrow(() -> new NotFoundResourceException("No se encontraron nodos cercanos"));
		final var condistancia = new NodoConDistancia();
		condistancia.setNodo(nodo);
		final var distancia = Math.sqrt(Math.pow(nodo.getLatitud() - latitud, 2) + Math.pow(nodo.getLongitud() - longitud, 2));
		condistancia.setDinstancia(distancia);
		return condistancia;
	}
	
}
