package ar.com.quibar.fravega.nodo.domain.dto;

import ar.com.quibar.fravega.nodo.domain.model.Nodo;

public class NodoConDistancia {

	private Nodo nodo;
	private double dinstancia;
	public Nodo getNodo() {
		return nodo;
	}
	public double getDinstancia() {
		return dinstancia;
	}
	public void setNodo(Nodo nodo) {
		this.nodo = nodo;
	}
	public void setDinstancia(double dinstancia) {
		this.dinstancia = dinstancia;
	}
	
}
