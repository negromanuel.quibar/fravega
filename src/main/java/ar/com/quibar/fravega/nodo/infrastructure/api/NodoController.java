package ar.com.quibar.fravega.nodo.infrastructure.api;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ar.com.quibar.fravega.nodo.domain.dto.NodoConDistancia;
import ar.com.quibar.fravega.nodo.domain.model.Nodo;
import ar.com.quibar.fravega.nodo.infrastructure.dto.PuntoRetiroRequest;
import ar.com.quibar.fravega.nodo.infrastructure.dto.SucursalRequest;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/nodo")
@RequiredArgsConstructor
@Slf4j
public class NodoController {

	private final NodoService service;
	
	@ApiOperation(value="Endpoint para buscar un Nodo")
	@GetMapping("/{id}")
	public ResponseEntity<Nodo> find(@Valid @PathVariable Integer id){
		log.info("GET /nodo/{}",id);
		return service.find(id);
	}
	@ApiOperation(value="Endpoint para buscar un Nodo")
	@GetMapping("/near")
	public ResponseEntity<NodoConDistancia> near(@RequestParam(name = "lat") Long latitud, @RequestParam(name = "lng") Long longitud){
		return service.near(latitud, longitud);
	}
	
	@ApiOperation(value="Endpoint para crear una Sucursal")
	@PostMapping("/sucursal")
	public ResponseEntity<Nodo> create(@Valid @RequestBody SucursalRequest request){
		log.info("PUT /nodo/sucursal request: {}", request);
		return service.create(request);
	}
	
	@ApiOperation(value="Endpoint para crear una Punto de Retiro")
	@PostMapping("/puntoretiro")
	public ResponseEntity<Nodo> create(@Valid @RequestBody PuntoRetiroRequest request){
		log.info("PUT /nodo/puntoretiro request: {}", request);
		return service.create(request);
	}
	
	@ApiOperation(value="Endpoint para modificar una Sucursal")
	@PatchMapping("/sucursal/{id}")
	public ResponseEntity<Nodo> update(@Valid @PathVariable(name="id",required = true) Integer id,
			@RequestBody SucursalRequest request){
		log.info("PATCH /nodo/sucursal/{id} request: {}", id, request);
		return service.update(id, request);
	}
	
	@ApiOperation(value="Endpoint para modificar una Punto de Retiro")
	@PostMapping("/puntoretiro/{id}")
	public ResponseEntity<Nodo> update(@Valid @PathVariable(name="id",required = true) Integer id,
			@RequestBody PuntoRetiroRequest request){
		log.info("PATCH /nodo/puntoretiro/{id} request: {}", id, request);
		return service.update(id,request);
	}
	
	
	@ApiOperation(value="Endpoint para modificar una Sucursal")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@Valid @PathVariable(name="id",required = true) Integer id){
		log.info("DELETE /nodo/{id}", id);
		return service.delete(id);
	} 
	
	
}
