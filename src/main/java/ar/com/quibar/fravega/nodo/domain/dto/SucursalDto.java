package ar.com.quibar.fravega.nodo.domain.dto;

public class SucursalDto extends NodoDto {
	
	private String direccion;
	private String horarioAtencion;

	
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getHorarioAtencion() {
		return horarioAtencion;
	}

	public void setHorarioAtencion(String horarioAtencion) {
		this.horarioAtencion = horarioAtencion;
	}
	

}
