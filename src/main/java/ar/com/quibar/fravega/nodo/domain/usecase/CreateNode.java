package ar.com.quibar.fravega.nodo.domain.usecase;

import org.springframework.beans.BeanUtils;

import ar.com.quibar.fravega.nodo.domain.dto.PuntoRetiroDto;
import ar.com.quibar.fravega.nodo.domain.dto.SucursalDto;
import ar.com.quibar.fravega.nodo.domain.model.Nodo;
import ar.com.quibar.fravega.nodo.domain.model.PuntoRetiro;
import ar.com.quibar.fravega.nodo.domain.model.Sucursal;
import ar.com.quibar.fravega.nodo.domain.port.NodoRepository;

public class CreateNode {

	private final NodoRepository<Nodo> repository;

	public CreateNode(NodoRepository<Nodo> repository) {
		this.repository = repository;
	}
	
	public Nodo createNodo(SucursalDto sucursalDto) {
		final var sucursal = new Sucursal();
		BeanUtils.copyProperties(sucursalDto, sucursal);
		return (Nodo) repository.save(sucursal);
	}
	
	public Nodo createNodo(PuntoRetiroDto ptoRetiroDto) {
		final var ptoRetiro = new PuntoRetiro();
		BeanUtils.copyProperties(ptoRetiroDto, ptoRetiro);
		return (Nodo) repository.save(ptoRetiro);
	}
	
}
