package ar.com.quibar.fravega.nodo.domain.port;

import java.util.List;
import java.util.Optional;

import ar.com.quibar.fravega.nodo.domain.model.Nodo;

public interface NodoRepository<T extends Nodo> {

	Optional<T> findById(Integer id);
	
	T save(T nodo);
	
	void delete(T nodo);
	
	List<T> findAll();
	
	Optional<T> findOneNearby(Long latitud, Long longitud);
}
