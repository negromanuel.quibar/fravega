package ar.com.quibar.fravega.nodo.infrastructure.api;

import static org.springframework.http.ResponseEntity.ok;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import ar.com.quibar.fravega.nodo.domain.dto.NodoConDistancia;
import ar.com.quibar.fravega.nodo.domain.dto.PuntoRetiroDto;
import ar.com.quibar.fravega.nodo.domain.dto.SucursalDto;
import ar.com.quibar.fravega.nodo.domain.model.Nodo;
import ar.com.quibar.fravega.nodo.domain.usecase.CreateNode;
import ar.com.quibar.fravega.nodo.domain.usecase.DeleteNodo;
import ar.com.quibar.fravega.nodo.domain.usecase.FindNode;
import ar.com.quibar.fravega.nodo.domain.usecase.UpdateNodo;
import ar.com.quibar.fravega.nodo.infrastructure.dto.PuntoRetiroRequest;
import ar.com.quibar.fravega.nodo.infrastructure.dto.SucursalRequest;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class NodoService {

	private final CreateNode create;
	private final DeleteNodo delete;
	private final FindNode find;
	private final UpdateNodo update;
	
	
	public ResponseEntity<Nodo> create(SucursalRequest request) {
		final var dto = new SucursalDto();
		BeanUtils.copyProperties(request, dto);
		
		return ok(create.createNodo(dto));
	}
	public ResponseEntity<Nodo> create(PuntoRetiroRequest request) {
		final var dto = new PuntoRetiroDto();
		BeanUtils.copyProperties(request, dto);
		return ok(create.createNodo(dto));
	}
	public ResponseEntity<Nodo> find( Integer id) {
		return ok(find.find(id));
	}
	public ResponseEntity<Nodo> update(Integer id, SucursalRequest request) {
		final var dto = new SucursalDto();
		BeanUtils.copyProperties(request, dto);
		return ok(update.update(id, dto));
	}
	public ResponseEntity<Nodo> update(Integer id, PuntoRetiroRequest request) {
		final var dto = new PuntoRetiroDto();
		BeanUtils.copyProperties(request, dto);
		return ok(update.update(id, dto));
	}
	public ResponseEntity<Void> delete(Integer id) {
		final var nodo = find.find(id);
		delete.delete(nodo);
		
		return ok().build();
	}
	public ResponseEntity<NodoConDistancia> near(Long latitud, Long longitud) {
		return ok(find.findNearby(latitud, longitud));
	}
	
	
}
