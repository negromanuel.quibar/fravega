package ar.com.quibar.fravega.nodo.domain.usecase;

import org.springframework.beans.BeanUtils;

import ar.com.quibar.fravega.nodo.domain.dto.PuntoRetiroDto;
import ar.com.quibar.fravega.nodo.domain.dto.SucursalDto;
import ar.com.quibar.fravega.nodo.domain.model.Nodo;
import ar.com.quibar.fravega.nodo.domain.model.PuntoRetiro;
import ar.com.quibar.fravega.nodo.domain.model.Sucursal;
import ar.com.quibar.fravega.nodo.domain.port.NodoRepository;

public class UpdateNodo {

	private final NodoRepository<Nodo> repository;

	public UpdateNodo(NodoRepository<Nodo> repository) {
		this.repository = repository;
	}
	
	public Nodo update(int id, SucursalDto sucursalDto) {
		final var sucursal = (Sucursal)repository.findById(id).orElseThrow();
		BeanUtils.copyProperties(sucursalDto, sucursal);
		return repository.save(sucursal);
	}
	
	public Nodo update(int id, PuntoRetiroDto ptoRetiroDto) {
		final var ptoRetiro = (PuntoRetiro)repository.findById(id).orElseThrow();
		BeanUtils.copyProperties(ptoRetiroDto, ptoRetiro);
		return repository.save(ptoRetiro);
	}
}
