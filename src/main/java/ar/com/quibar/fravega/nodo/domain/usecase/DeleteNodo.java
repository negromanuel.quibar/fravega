package ar.com.quibar.fravega.nodo.domain.usecase;

import ar.com.quibar.fravega.nodo.domain.model.Nodo;
import ar.com.quibar.fravega.nodo.domain.port.NodoRepository;

public class DeleteNodo {

	private final NodoRepository<Nodo> repository;

	public DeleteNodo(NodoRepository<Nodo> repository) {
		this.repository = repository;
	}
	
	public void delete(Nodo nodo) {
		repository.delete(nodo);
	}
}
